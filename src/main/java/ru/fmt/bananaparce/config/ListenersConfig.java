package ru.fmt.bananaparce.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.fmt.bananaparce.listeners.*;
import ru.fmt.bananaparce.repo.*;

import javax.annotation.PostConstruct;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

@Configuration
@Slf4j
public class ListenersConfig {

    private Set<String> pairs = new HashSet<>();

    @PostConstruct
    public void initPairs() {
        pairs.add("XEMBTC");
    }

    @Bean
    Set<Listener> allListeners(
            ObjectMapper jacksonMapper,
            TradeRepo tradeRepo,
            AggTradeRepo aggTradeRepo,
            DepthRepo depthRepo,
            DepthSnapshotRepo depthSnapshotRepo){
        Set<Listener> listeners = new HashSet<>();

        pairs.forEach(pair -> {
            try {
                val aggTradeListener = new AggTradeListener(pair, aggTradeRepo, jacksonMapper);
                listeners.add(aggTradeListener);

                val tradeListener = new TradeListener(pair, tradeRepo, jacksonMapper);
                listeners.add(tradeListener);

                val depthListener = new DepthListener(pair, jacksonMapper,  depthRepo, depthSnapshotRepo);
                listeners.add(depthListener);

            } catch (URISyntaxException ex) {
                log.warn("Pair with name {} cant create listener", pair);
            }
        });

        return listeners;
    }

}
