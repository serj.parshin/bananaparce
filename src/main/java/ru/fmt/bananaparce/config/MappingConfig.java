package ru.fmt.bananaparce.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MappingConfig {

    @Bean
    public ObjectMapper jacksonMapper() {
        return new ObjectMapper();
    }
}
