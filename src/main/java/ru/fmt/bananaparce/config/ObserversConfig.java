package ru.fmt.bananaparce.config;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.fmt.bananaparce.controller.ListenerObserver;
import ru.fmt.bananaparce.listeners.Listener;

import java.util.HashSet;
import java.util.Set;

@Configuration
@Slf4j
public class ObserversConfig {
    @Bean
    Set<ListenerObserver> listenersObservers(Set<Listener> allListeners){
        Set<ListenerObserver> observers = new HashSet<>();

        allListeners.forEach(listener -> {
            val observer = new ListenerObserver(listener);
            observers.add(observer);
        });

        observers.forEach(Thread::start);

        return observers;
    }
}
