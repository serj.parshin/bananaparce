package ru.fmt.bananaparce.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.fmt.bananaparce.domain.Depth;

@Repository
public interface DepthRepo extends JpaRepository<Depth, Long> {
}
