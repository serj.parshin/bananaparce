package ru.fmt.bananaparce.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.fmt.bananaparce.domain.Bid;

public interface BidRepo extends JpaRepository<Bid, Long> {
}
