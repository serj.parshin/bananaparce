package ru.fmt.bananaparce.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.fmt.bananaparce.domain.Ask;

public interface AskRepo extends JpaRepository<Ask, Long> {
}
