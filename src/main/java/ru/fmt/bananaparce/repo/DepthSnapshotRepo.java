package ru.fmt.bananaparce.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.fmt.bananaparce.domain.DepthSnapshot;

@Repository
public interface DepthSnapshotRepo extends JpaRepository<DepthSnapshot, Long> {
}
