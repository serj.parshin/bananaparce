package ru.fmt.bananaparce.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.fmt.bananaparce.domain.AggTrade;

import java.util.Optional;

@Repository
public interface AggTradeRepo extends JpaRepository<AggTrade, Long> {
    Optional<AggTrade> findBySymbol(String symbol);
    Optional<AggTrade> findByIdAndSymbol(Long id, String symbol);
}
