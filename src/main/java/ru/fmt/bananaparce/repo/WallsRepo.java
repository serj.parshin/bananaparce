package ru.fmt.bananaparce.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.fmt.bananaparce.domain.Walls;

@Repository
public interface WallsRepo extends JpaRepository<Walls, Long> {
}
