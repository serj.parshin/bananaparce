package ru.fmt.bananaparce.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.fmt.bananaparce.domain.Trade;

import java.util.Optional;

@Repository
public interface TradeRepo extends JpaRepository<Trade, Long> {
    Optional<Trade> findBySymbol(String symbol);
    Optional<Trade> findByIdAndSymbol(Long id, String symbol);
}
