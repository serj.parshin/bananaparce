package ru.fmt.bananaparce.listeners;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsunsoft.http.HttpRequest;
import com.jsunsoft.http.HttpRequestBuilder;
import com.jsunsoft.http.ResponseHandler;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.java_websocket.handshake.ServerHandshake;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.fmt.bananaparce.controller.DepthSnapshotUpdater;
import ru.fmt.bananaparce.domain.*;
import ru.fmt.bananaparce.repo.DepthSnapshotRepo;
import ru.fmt.bananaparce.repo.DepthRepo;
import ru.fmt.bananaparce.repo.WallsRepo;

import java.io.IOException;
import java.net.URISyntaxException;

@Slf4j
public class DepthListener extends Listener {
    private static final String SUFFIX = "@depth";

    private ObjectMapper mapper;
    private JpaRepository depthRepo;
    private DepthSnapshotUpdater snapshotUpdater;

    public DepthListener(String pair,
                         ObjectMapper mapper,
                         @Qualifier("depthRepo") JpaRepository depthRepo,
                         @Qualifier("depthSnapshotRepo") JpaRepository snapshotRepo
    ) throws URISyntaxException {
        super(pair, SUFFIX, depthRepo, mapper, Depth.class);
        this.mapper = mapper;
        this.depthRepo = depthRepo;
        this.snapshotUpdater = new DepthSnapshotUpdater(pair, "1000", snapshotRepo, mapper);
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        snapshotUpdater.update();
        if(isActive()){
            log.info("Connected to: {}", super.uri);
        } else {
            log.info("Reconnected to: {}", super.uri);
        }
    }

    @Override
    public void onMessage(String message) {
        log.info("[ -= GOT =- ]: {}", message);
        try {
            Depth depth = mapper.readValue(message, Depth.class);
            depthRepo.save(depth);
        } catch (IOException e) {
            log.warn("Error when saving massage {}", message);
            e.printStackTrace();
        }
    }

}
