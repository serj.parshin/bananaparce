package ru.fmt.bananaparce.listeners;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import ru.fmt.bananaparce.domain.AggTrade;
import ru.fmt.bananaparce.repo.AggTradeRepo;

import java.net.URISyntaxException;

@Slf4j
public class AggTradeListener extends Listener {

    private static final String SUFFIX = "@aggTrade";

    public AggTradeListener(String pairName, AggTradeRepo aggTradeRepo, ObjectMapper mapper) throws URISyntaxException {
        super(pairName, SUFFIX, aggTradeRepo, mapper, AggTrade.class);
    }

}
