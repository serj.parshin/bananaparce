package ru.fmt.bananaparce.listeners;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import ru.fmt.bananaparce.domain.Trade;
import ru.fmt.bananaparce.repo.TradeRepo;

import java.net.URISyntaxException;

@Slf4j
public class TradeListener extends Listener {

    private static final String SUFFIX = "@trade";

    public TradeListener(String pairName, TradeRepo tradeRepo, ObjectMapper mapper) throws URISyntaxException {
        super(pairName, SUFFIX, tradeRepo, mapper, Trade.class);
    }

}
