package ru.fmt.bananaparce.listeners;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@Slf4j
public class Listener extends WebSocketClient {

    private static final String URL = "wss://stream.binance.com:9443/ws/";

    private Boolean active;
    private ObjectMapper mapper;
    private JpaRepository abstractRepo;
    private Class classToMapping;

    public Listener(
            String pairName,
            String suffix,
            JpaRepository abstractRepo,
            ObjectMapper mapper,
            Class classToMapping
    ) throws URISyntaxException {
        super(new URI(URL + pairName.toLowerCase() + suffix));
        this.abstractRepo = abstractRepo;
        this.mapper = mapper;
        this.classToMapping = classToMapping;
        this.active = true;
        super.setReuseAddr(true);
        super.connect();
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        if(isActive()){
            log.info("Connected to: {}", super.uri);
        } else {
            log.info("Reconnected to: {}", super.uri);
        }
    }

    @Override
    public void onMessage(String message) {
        log.info("[ -= GOT =- ]: {}", message);
        try {
            this.abstractRepo.save(mapper.readValue(message, classToMapping));
        } catch (IOException e) {
            log.warn("Error when saving massage {}", message);
            e.printStackTrace();
        }
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        setActive(false);
        log.info("Disconnected to pair: {}", super.getURI());
    }

    @Override
    public void onError(Exception e) {
        setActive(false);
        log.warn("Error {}", e.getMessage());
    }
}
