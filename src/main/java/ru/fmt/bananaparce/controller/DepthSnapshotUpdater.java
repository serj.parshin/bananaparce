package ru.fmt.bananaparce.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsunsoft.http.HttpRequest;
import com.jsunsoft.http.HttpRequestBuilder;
import com.jsunsoft.http.ResponseHandler;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.fmt.bananaparce.domain.DepthSnapshot;

import java.io.IOException;

@Slf4j
public class DepthSnapshotUpdater {
    static final String PREFIX = "https://www.binance.com/api/v1/depth?";

    private HttpRequest<JsonNode> httpsRequest;
    private JpaRepository snapshotRepo;
    private ObjectMapper mapper;
    private String pair;

    public DepthSnapshotUpdater(
            String pair, String limit,
            @Qualifier("depthSnapshotRepo") JpaRepository depthSnapshotRepo,
            ObjectMapper mapper
    ){
        this.httpsRequest = HttpRequestBuilder.createGet(
                PREFIX + "symbol=" + pair + "&limit=" + limit,
                JsonNode.class
        ).build();
        this.snapshotRepo = depthSnapshotRepo;
        this.mapper = mapper;
        this.pair = pair;
    }

    public void update(){
        ResponseHandler<JsonNode> responseHandler = httpsRequest.execute();
        val book = String.valueOf(responseHandler.get());
        try {
            DepthSnapshot depthSnapshot = mapper.readValue(book, DepthSnapshot.class);
            depthSnapshot.setSymbol(pair);
            snapshotRepo.save(depthSnapshot);
        } catch (IOException e) {
            log.warn("Error when saving Depth Snapshot: ");
            e.printStackTrace();
        }
    }

}
