package ru.fmt.bananaparce.controller;

import lombok.extern.slf4j.Slf4j;
import ru.fmt.bananaparce.listeners.Listener;

@Slf4j
public class ListenerObserver extends Thread {

    private Listener listener;
    public ListenerObserver(Listener listener){
        this.listener = listener;
    }

    @Override
    public void run(){
        watchListener();
    }

    private void watchListener() {
        while(true) {
            if (listener.isActive()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                log.info("Try to reconnect: {}", listener.getURI());
                listener.reconnect();
            }
        }
    }

}
