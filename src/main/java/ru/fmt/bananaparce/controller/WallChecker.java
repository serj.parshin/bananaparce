package ru.fmt.bananaparce.controller;

import lombok.val;
import ru.fmt.bananaparce.domain.*;

import java.io.IOException;
import java.util.Iterator;

public class WallChecker {
    /*try {
        Depth depth = mapper.readValue(message, Depth.class);
        Walls wall = new Walls();

        responseHandler = httpsRequest.execute();
        val book = String.valueOf(responseHandler.get());
        DepthSnapshot readedBook = mapper.readValue(book, DepthSnapshot.class);

        for (Ask depthAsk:depth.getAsks()) {
            int counter = 0;
            for(Iterator<Ask> bookAskIterator = readedBook.getAsks().iterator(); bookAskIterator.hasNext();){

                Ask bookAsk = (Ask) bookAskIterator.next();

                if (depthAsk.getPrice() == bookAsk.getPrice()){
                    if(depthAsk.getQuantity() == "0.00000000"){
                        bookAskIterator.remove();
                    } else {
                        bookAsk.setQuantity(depthAsk.getQuantity());
                    }
                } else {
                    counter++;
                }
            }
            if(counter >= 1){
                readedBook.getAsks().add(depthAsk);
            }
        }

        for (Bid depthBid:depth.getBids()) {
            int counter = 0;
            for(Iterator<Bid> bookBidIterator = readedBook.getBids().iterator();bookBidIterator.hasNext();){

                Bid bookBid = (Bid) bookBidIterator.next();

                if (depthBid.getPrice() == bookBid.getPrice()){
                    if(depthBid.getQuantity() == "0.00000000"){
                        bookBidIterator.remove();
                    } else {
                        bookBid.setQuantity(depthBid.getQuantity());
                    }
                } else {
                    counter++;
                }
            }
            if(counter >= 1){
                readedBook.getBids().add(depthBid);
            }
        }


        Double askDelta = 0.0;
        Ask askToWall = null;
        int i = 0;

        for (Ask ask:readedBook.getAsks()) {
            askDelta += Double.valueOf(ask.getQuantity());
            i++;
        }

        askDelta /= i;
        log.info("Ask delta: " + askDelta.toString());

        for (Ask ask:readedBook.getAsks()) {
            if(Double.valueOf(ask.getQuantity()) >= askDelta){
                log.info("Ask to wall: " + ask.toString());
                wall.setAsk(ask);
                break;
            }
        }

        Double bidDelta = 0.0;
        Bid bidToWall = null;
        i = 0;

        for (Bid bid:readedBook.getBids()) {
            bidDelta += Double.valueOf(bid.getQuantity());
            i++;
        }

        bidDelta /= i;
        log.info("Bid delta: " + bidDelta.toString());

        for (Bid bid:readedBook.getBids()) {
            if(Double.valueOf(bid.getQuantity()) >= bidDelta){
                log.info("Bid to wall: " + bid.toString());
                wall.setBid(bid);
                break;
            }
        }


        wall.setEventTime(depth.getEventTime());
        log.info("Wall to save: " + wall.toString());
        this.wallsRepo.save(wall);

    } catch (
    IOException e) {
        log.warn("Error when saving massage {}", message);
        e.printStackTrace();
    }*/
}
