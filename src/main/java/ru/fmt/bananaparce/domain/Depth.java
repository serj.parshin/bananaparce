package ru.fmt.bananaparce.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
@Data
@EqualsAndHashCode(of = {"id", "eventTime"})
public class Depth {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonProperty("e")
    private String eventType;

    @JsonProperty("E")
    private Long eventTime;

    @JsonProperty("s")
    private String symbol;

    @JsonProperty("U")
    private Long firstUpdateId;

    @JsonProperty("u")
    private Long finalUpdateId;

    @JsonProperty("b")
    @OneToMany(cascade = {CascadeType.ALL})
    @JsonBackReference(value = "bids")
    private List<Bid> bids;

    @JsonProperty("a")
    @OneToMany(cascade = {CascadeType.ALL})
    @JsonBackReference(value = "asks")
    private List<Ask> asks;
}
