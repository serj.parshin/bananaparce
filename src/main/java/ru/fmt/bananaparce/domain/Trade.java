package ru.fmt.bananaparce.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table
@Data
@EqualsAndHashCode(of = {"id", "tradeID", "eventTime"})
public class Trade {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonProperty("e")
    private String eventType;

    @JsonProperty("E")
    private Long eventTime;

    @JsonProperty("s")
    private String symbol;

    @JsonProperty("t")
    private Long tradeID;

    @JsonProperty("p")
    private String price;

    @JsonProperty("q")
    private String quantity;

    @JsonProperty("b")
    private Long buyerOrderID;

    @JsonProperty("a")
    private Long sellerOrderID;

    @JsonProperty("T")
    private Long tradeTime;

    @JsonProperty("m")
    private boolean isTheBuyerTheMarketMaker;

    @JsonProperty("M")
    private boolean ignore;

}