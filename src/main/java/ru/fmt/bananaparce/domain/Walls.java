package ru.fmt.bananaparce.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table
@Data
@EqualsAndHashCode(of = {"id", "eventTime"})
public class Walls {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long eventTime;

    @OneToOne(cascade = {CascadeType.ALL})
    private Bid bid;

    @OneToOne(cascade = {CascadeType.ALL})
    private Ask ask;
}
