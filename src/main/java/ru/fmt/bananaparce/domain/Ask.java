package ru.fmt.bananaparce.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.fmt.bananaparce.domain.deserializer.AskDeserializer;

import javax.persistence.*;

@Entity
@Table
@Data
@JsonDeserialize(using = AskDeserializer.class)
@EqualsAndHashCode(of = {"id", "price"})
public class Ask {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String price;
    private String quantity;

    public Ask(String price, String quantity){
        this.price = price;
        this.quantity = quantity;
    }

}