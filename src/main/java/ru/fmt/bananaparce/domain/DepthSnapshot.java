package ru.fmt.bananaparce.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
@Data
@EqualsAndHashCode(of = {"id", "symbol"})
public class DepthSnapshot {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String symbol;

    @JsonProperty("lastUpdateId")
    private String lastUpdateId;

    @JsonProperty("bids")
    @OneToMany(cascade = {CascadeType.ALL})
    private List<Bid> bids;

    @JsonProperty("asks")
    @OneToMany(cascade = {CascadeType.ALL})
    private List<Ask> asks;
}
