package ru.fmt.bananaparce.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table
@Data
@EqualsAndHashCode(of = {"id", "aggregateTradeID", "eventTime"})
public class AggTrade {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonProperty("e")
    private String eventType;

    @JsonProperty("E")
    private Long eventTime;

    @JsonProperty("s")
    private String symbol;

    @JsonProperty("a")
    private Long aggregateTradeID;

    @JsonProperty("p")
    private String price;

    @JsonProperty("q")
    private String quantity;

    @JsonProperty("f")
    private Long firstTradeID;

    @JsonProperty("l")
    private Long lastTradeID;

    @JsonProperty("T")
    private Long tradeTime;

    @JsonProperty("m")
    private boolean isTheBuyerTheMarketMaker;

    @JsonProperty("M")
    private boolean ignore;
}
