package ru.fmt.bananaparce.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.fmt.bananaparce.domain.deserializer.BidDeserializer;

import javax.persistence.*;

@Entity
@Table
@Data
@JsonDeserialize(using = BidDeserializer.class)
@EqualsAndHashCode(of = {"id", "price"})
public class Bid {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String price;
    private String quantity;

    public Bid(String price, String quantity){
        this.price = price;
        this.quantity = quantity;
    }

}
