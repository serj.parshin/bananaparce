package ru.fmt.bananaparce.domain.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import ru.fmt.bananaparce.domain.Bid;

import java.io.IOException;

public class BidDeserializer extends StdDeserializer<Bid> {
    public BidDeserializer() {
        this(null);
    }

    public BidDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Bid deserialize(
            JsonParser jsonParser,
            DeserializationContext deserializationContext
    ) throws IOException, JsonProcessingException {
        JsonNode bidNode = jsonParser.getCodec().readTree(jsonParser);

        Bid bid = new Bid(bidNode.get(0).asText(), bidNode.get(1).asText());

        return bid;
    }
}
