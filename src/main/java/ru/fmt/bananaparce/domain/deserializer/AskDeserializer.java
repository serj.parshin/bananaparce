package ru.fmt.bananaparce.domain.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import ru.fmt.bananaparce.domain.Ask;

import java.io.IOException;

public class AskDeserializer extends StdDeserializer<Ask> {
    public AskDeserializer() {
        this(null);
    }

    public AskDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Ask deserialize(
            JsonParser jsonParser,
            DeserializationContext deserializationContext
    ) throws IOException, JsonProcessingException {
        JsonNode asksNode = jsonParser.getCodec().readTree(jsonParser);

        Ask ask = new Ask(asksNode.get(0).asText(), asksNode.get(1).asText());

        return ask;
    }
}
