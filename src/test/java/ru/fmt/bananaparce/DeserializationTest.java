package ru.fmt.bananaparce;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import ru.fmt.bananaparce.domain.Kline;
import ru.fmt.bananaparce.domain.PartialDepth;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

@SpringBootTest
@Slf4j
public class DeserializationTest {

    @Test
    public void loadResources(){
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("kline.json").getFile());

        System.out.println(file.getAbsolutePath());

        assertEquals(file.getName(), "kline.json");
    }

    /*@Test
    public void klineDeserialization() throws IOException {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("kline.json").getFile());
        String klineToTest = new String(Files.readAllBytes(file.toPath()));

        ObjectMapper mapper = new ObjectMapper();

        Kline readValue = mapper.readValue(klineToTest, Kline.class);

        assertNotNull(readValue);

        assertThat(readValue.getEventType(), equalTo("kline"));
        assertThat(readValue.getEventTime(), equalTo(Long.valueOf("1554715060123")));
        assertThat(readValue.getSymbol(), equalTo("BNBBTC"));

        assertThat(readValue.getStartTime(), equalTo(Long.valueOf("1554715020000")));
        assertThat(readValue.getCloseTime(), equalTo(Long.valueOf("1554715079999")));
        assertThat(readValue.getInterval(), equalTo("1m"));

        assertThat(readValue.getFirstTradeId(), equalTo(Long.valueOf("112248484")));
        assertThat(readValue.getLastTradeId(), equalTo(Long.valueOf("112248579")));

        assertThat(readValue.getOpenPrice(), equalTo("0.0010"));
        assertThat(readValue.getClosePrice(), equalTo("0.0020"));
        assertThat(readValue.getHighPrice(), equalTo("0.0025"));
        assertThat(readValue.getLowPrice(), equalTo("0.0015"));

        assertThat(readValue.getBaseAssetVolume(), equalTo("1000"));
        assertThat(readValue.getNumberOfTrades(), equalTo(Long.valueOf("96")));

        assertThat(readValue.getIsThisKlineClosed(), equalTo(false));

        assertThat(readValue.getQuoteAssetVolume(), equalTo("1.0000"));
        assertThat(readValue.getTakerBuyBaseAssetVolume(), equalTo("500"));
        assertThat(readValue.getTakerBuyQuoteAssetVolume(), equalTo("0.500"));
        assertThat(readValue.getIgnore(), equalTo("123456"));
    }
*/
    @Test
    public void partialDepthDeserialization() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("partialDepth.json").getFile());
        String depth = new String(Files.readAllBytes(file.toPath()));

        ObjectMapper mapper = new ObjectMapper();

        PartialDepth readValue = mapper.readValue(depth, PartialDepth.class);

        log.info(readValue.toString());
        assertNotNull(readValue);
    }
}
